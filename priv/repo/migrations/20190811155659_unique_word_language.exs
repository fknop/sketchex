defmodule Sketch.Repo.Migrations.UniqueWordLanguage do
  use Ecto.Migration

  def change do
    create unique_index(:words, [:word, :language])
  end
end
