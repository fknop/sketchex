defmodule Sketch.Repo.Migrations.AlterWordsLanguage do
  use Ecto.Migration

  def change do
    alter table(:words) do
      modify :language,
              references(:languages, [column: :code, type: :string, on_delete: :delete_all]),
              [default: "en", from: :string]
    end
  end
end
