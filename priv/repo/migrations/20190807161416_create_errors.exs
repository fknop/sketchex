defmodule Sketch.Repo.Migrations.CreateErrors do
  use Ecto.Migration

  def change do
    create table(:errors) do
      add :name, :string
      add :message, :string
      add :stack, :text
      add :language, :string 
      add :platform, :string 
      add :user_agent, :string

      timestamps()
    end
  end
end
