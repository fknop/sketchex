languages = [
  %{name: "English", code: "en"},
  %{name: "French", code: "fr"}
]


for language <- languages do
  Sketch.Repo.insert!(%Sketch.Language{
    name: language.name,
    code: language.code
  })
end
