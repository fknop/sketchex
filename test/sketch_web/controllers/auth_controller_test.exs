defmodule SketchWeb.AuthControllerTest do
  use SketchWeb.ConnCase

  test "signup/2: bad_request (missing parameters)", %{conn: conn} do

    response = 
      conn 
      |> post(Routes.auth_path(conn, :signup))
      |> json_response(400)

    errors = %{
      "errors" => %{
        "email" => [%{"validation" => "required"}],
        "password" => [%{"validation" => "required"}],
        "username" => [%{"validation" => "required"}]
      }
    }

    assert response == errors
  end

  test "signup/2: bad_request (bad parameters)", %{conn: conn} do

    response = 
      conn 
      |> post(Routes.auth_path(conn, :signup), %{"username" => "ab", "email" => "test", "password" => "abc"})
      |> json_response(400)

      errors = %{
        "errors" => %{
          "email" => [%{"validation" => "format"}],
          "password" => [%{"validation" => "length", "count" => 5, "kind" => "min", "type" => "string"}],
          "username" => [%{"validation" => "length", "count" => 3, "kind" => "min", "type" => "string"}]
        }
      }

      assert errors == response
  end

  test "signup/2: created", %{conn: conn} do

    body = %{"username" => "John", "password" => "super_secure_password", "email" => "john@domain.com"}

    response = 
      conn 
      |> post(Routes.auth_path(conn, :signup), body)
      |> json_response(201)

    assert %{ "id" => id, "token" => token } = response
    
    {:ok, user} = Sketch.User.Query.get_by_id(id)
    
    assert user.id == id 
    assert user.username == body["username"] 
    assert user.email == body["email"]
  end

  test "signin/2: bad_request", %{conn: conn} do
    response = 
      conn 
      |> post(Routes.auth_path(conn, :signin))
      |> json_response(400)
  end

  test "signin/2: unauthorized", %{conn: conn} do
    user = %{"email" => "test@test.com", "password" => "test"}

    response = 
      conn 
      |> post(Routes.auth_path(conn, :signin), user)
      |> json_response(401)

    assert %{"reason" => "invalid"} = response
  end

  test "signin/2: authorized", %{conn: conn} do

    body = %{"username" => "user_test", "email" => "test@test.com", "password" => "long_password"}

    {:ok, user} = Sketch.User.Query.create_user(body)

    response = 
      conn 
      |> post(Routes.auth_path(conn, :signin), body)
      |> json_response(200)

    assert %{"token" => token, "id" => id} = response
    assert id == user.id
  end

  test "signin/2: guest", %{conn: conn} do

    body = %{"username" => "Bob"}

    response = 
      conn 
      |> post(Routes.auth_path(conn, :signin), body)
      |> json_response(200)

    assert %{"token" => token, "id" => id} = response
  end

end