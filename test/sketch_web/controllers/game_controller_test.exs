defmodule SketchWeb.GameControllerTest do
  use SketchWeb.ConnCase

  import Phoenix.ConnTest


  setup %{conn: conn} do
    body = %{"username" => "Guest"}

    %{"token" => token} =
      conn
      |> post(Routes.auth_path(conn, :signin), body)
      |> json_response(200)


    {:ok, conn: put_req_header(conn, "authorization", "Bearer " <> token)}
  end

  test "create game with no parameters", %{conn: conn} do

    response =
      conn
      |> post(Routes.game_path(conn, :create), %{})
      |> json_response(400)
  end

  test "create game", %{conn: conn} do

    parameters = %{
      "name" => "TestServer",
      "turn_timeout" => 90,
      "reveal_time" => 15,
      "min_players" => 2,
      "autostart" => true,
      "allow_join" => false,
      "private" => false
    }

    %{"game_id" => id} =
      conn
      |> post(Routes.game_path(conn, :create), parameters)
      |> json_response(200)

    assert not is_nil(Sketch.GameServer.exists?(id))

    options = Sketch.GameServer.get_options(id)
    assert options.name == parameters["name"]
    assert options.turn_timeout == parameters["turn_timeout"]
    assert options.reveal_time == parameters["reveal_time"]
    assert options.min_players == parameters["min_players"]
    assert options.autostart == parameters["autostart"]
    assert options.allow_join == parameters["allow_join"]
    assert options.private == parameters["private"]
  end

end
