defmodule SketchWeb.DrawChannelTest do
  use SketchWeb.ChannelCase
  import Sketch.Crypto
  import Sketch.GameCreator

  alias SketchWeb.Auth
  alias Sketch.GameCreator
  alias Sketch.GameOptions

  @server_id "TestServer"
  @server_name "TestServerName"

  setup_all do
    uuid = player_uuid() # Temporary id
    {:ok, token, _} = Auth.Guardian.authenticate_guest(uuid, "Guest")

    {:ok, token: token, uuid: uuid}
  end

  setup_all %{uuid: uuid} do
    {:ok, _} = GameCreator.instantiate({ @server_id, @server_name, uuid, %GameOptions{} })
    :ok
  end

  setup_all %{token: token} do
    {:ok, socket} = connect(SketchWeb.UserSocket, %{ token: token }, %{})

    {:ok, socket: socket}
  end

  test "Join server", %{socket: socket} do
    {:ok, initial_state, socket} = subscribe_and_join(socket, "draw:" <> @server_id)
  end
end
