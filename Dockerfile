FROM elixir:1.9.1-alpine as build

RUN apk add --update build-base

RUN mkdir /app
WORKDIR /app 

RUN mix local.hex --force && \
    mix local.rebar --force 

ENV MIX_ENV=prod

COPY mix.exs ./
COPY config config


RUN mix do deps.get, deps.compile

COPY priv priv 
COPY lib lib

RUN mix do compile

COPY rel rel
RUN mix release

FROM alpine:3.9 as app 

RUN apk add --update bash openssl 

RUN mkdir /app
WORKDIR /app 

COPY --from=build /app/_build/prod/rel/sketch ./
RUN chown -R nobody: /app 
USER nobody 

ENV HOME=/app

CMD trap 'exit' INT; /app/bin/sketch start