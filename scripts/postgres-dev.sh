#!/bin/bash

VOLUME="$HOME/docker/volumes/postgres"

mkdir -p $VOLUME

docker run --rm --name postgres -e POSTGRES_PASSWORD="postgres" -d -p 5432:5432 -v $VOLUME:/var/lib/postgresql/data postgres:latest