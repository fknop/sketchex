#!/bin/bash


# Get last commit hash
TAG=$(git rev-parse HEAD)
NAME=registry.gitlab.com/fknop/sketchex
IMG=${NAME}:${TAG}
LATEST=${NAME}:latest

# Build image
docker build -t ${IMG} .
docker tag ${IMG} ${LATEST}

# Publish to GitLab
docker push ${NAME}