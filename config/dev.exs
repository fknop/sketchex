import Config

# For development, we disable any cache and enable
# debugging and code reloading.
#
# The watchers configuration can be used to run external
# watchers to your application. For example, we use it
# with webpack to recompile .js and .css sources.
config :sketch, SketchWeb.Endpoint,
  http: [port: System.get_env("PORT") || 4000],
  debug_errors: false,
  code_reloader: true,
  check_origin: false,
  watchers: []


config :libcluster,
  topologies: [
    sketch: [
      strategy: Elixir.Cluster.Strategy.Gossip
    ]
  ]


# Do not include metadata nor timestamps in development logs
config :logger, :console, format: "[$level] $message\n"

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime

# Configure your database
config :sketch, Sketch.Repo,
  username: "postgres",
  password: "root",
  database: "sketch_dev",
  hostname: "localhost",
  pool_size: 10



config :sketch, SketchWeb.Auth.Guardian,
  issuer: "sketch_web",
  secret_key: "2co3R6ljRxV2dwrL6eL4qDgYVPwc+BdarCXZOzDEydJ0BcU5yxiq2f0eaYX+HsHh"


config :cors_plug,
  origin: ["https://localhost:4200", "http://localhost:4200"]
