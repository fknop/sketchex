import Config

config :sketch, Sketch.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.fetch_env!("DATABASE_URL"),
  ssl: true,
  pool_size: 2

# config :sketch, SketchWeb.Endpoint,
#   server: true,
#   url: [host: System.fetch_env!("APP_NAME") <> ".gigalixirapp.com", port: 443]


config :sketch, SketchWeb.Auth.Guardian,
  issuer: "sketch_web",
  secret_key: System.fetch_env!("GUARDIAN_SECRET")


