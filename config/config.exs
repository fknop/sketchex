# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :sketch,
  ecto_repos: [Sketch.Repo]

# Configures the endpoint
config :sketch, SketchWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ijBtY63bIcLt/E1S3t0MnCEewMdWKXNaFja0oroeMD9R7AfCaRYmJ3kbNahLjkDz",
  render_errors: [view: SketchWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Sketch.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason


import_config "#{Mix.env()}.exs"
