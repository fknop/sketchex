import Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :sketch, SketchWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :sketch, Sketch.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "sketch_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox


config :sketch, SketchWeb.Auth.Guardian,
  issuer: "sketch_web",
  secret_key: "fQM04TnP9aP7GuEf6+TXMm+YYzaqMNAN31TQBsfef9VeJaAUAB0gT/EPcNDJsFkQ"


config :libcluster,
  topologies: [
    sketch: [
      strategy: Elixir.Cluster.Strategy.Gossip
    ]
  ]
