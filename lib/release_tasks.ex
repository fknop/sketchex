defmodule Sketch.ReleaseTasks do
  @app :sketch

  @start_apps [:logger, :ssl]

  defp init do
    Application.load(@app)
    Enum.each(@start_apps, &Application.ensure_all_started/1)
  end

  def migrate do
    init()

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def seed() do
    init()

    Ecto.Migrator.with_repo(Sketch.Repo, 
      fn _ -> 
        "#{seed_path(@app)}/*.exs"
        |> Path.wildcard()
        |> Enum.sort()
        |> Enum.each(&run_seed_script/1)
      end
    )
  end

  def seed_file(seed) do
    init()

    Ecto.Migrator.with_repo(Sketch.Repo, 
      fn _ -> 
        "#{seed_path(@app)}/#{seed}.exs"
        |> run_seed_script
      end
    )
  end

  def rollback(repo, version) do
    init()

    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp run_seed_script(seed_script) do
    IO.puts "Running seed script #{seed_script}.."
    Code.eval_file(seed_script)
  end

  defp seed_path(app),
    do: priv_dir(app, ["repo", "seeds"])
    
  defp priv_dir(app, path) when is_list(path) do
    case :code.priv_dir(app) do
      priv_path when is_list(priv_path) or is_binary(priv_path) ->
        Path.join([priv_path] ++ path)

      {:error, :bad_name} ->
        raise ArgumentError, "unknown application: #{inspect app}"
    end
  end
end