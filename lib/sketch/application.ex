defmodule Sketch.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository


      {Cluster.Supervisor, [Application.get_env(:libcluster, :topologies), [name: Sketch.ClusterSupervisor]]},

      # Add these 2 to one supervisor

      crdt([name: Sketch.Handoff.Crdt, sync_interval: 5, listener: Sketch.Handoff]),
      {Sketch.Handoff, [crdt: Sketch.Handoff.Crdt, shutdown: 10_000]},

      crdt([name: Sketch.Matchmaking.Crdt, sync_interval: 50, listener: Sketch.Matchmaking]),
      {Sketch.Matchmaking, [crdt: Sketch.Matchmaking.Crdt, shutdown: 10_000]},

      crdt([name: Sketch.GameRegistry.Crdt, sync_interval: 500, listener: Sketch.GameRegistry]),
      {Sketch.GameRegistry, [crdt: Sketch.GameRegistry.Crdt, shutdown: 10_000]},

      Sketch.Repo,
      # Start the endpoint when the application starts
      SketchWeb.Endpoint,
      SketchWeb.Presence,

      {Horde.Registry, [name: Sketch.GlobalRegistry, keys: :unique]},
      {Horde.Supervisor, [name: Sketch.GlobalSupervisor, strategy: :one_for_one, shutdown: 10_000]},
      {Sketch.NodeConnector, modules: [
        { Sketch.GlobalRegistry, &set_members/1 },
        { Sketch.GlobalSupervisor, &set_members/1 },
        { Sketch.Handoff.Crdt, &set_neighbours/1 },
        { Sketch.Matchmaking.Crdt, &set_neighbours/1 },
        { Sketch.GameRegistry.Crdt, &set_neighbours/1 },
      ]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Sketch.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp crdt(options) do
    listener = Keyword.get(options, :listener, nil)

    {
      DeltaCrdt,
      crdt: DeltaCrdt.AWLWWMap,
      name: Keyword.get(options, :name),
      sync_interval: Keyword.get(options, :sync_interval),
      on_diffs: fn diffs ->
        send(listener, {:crdt_update, diffs})
      end
    }
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    SketchWeb.Endpoint.config_change(changed, removed)
    :ok
  end


  defp set_members(name) do
    members = [Node.self() | Node.list()]
      |> Enum.map(fn node -> {name, node} end)
    :ok = Horde.Cluster.set_members(name, members)
  end

  defp set_neighbours(crdt) do
    neighbours = Node.list() |> Enum.map(&({ crdt, &1 }))
    DeltaCrdt.set_neighbours({crdt, Node.self()}, neighbours)
  end
end
