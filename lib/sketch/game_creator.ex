defmodule Sketch.GameCreator do

  import Sketch.Crypto

  alias Sketch.GameOptions


  def instantiate({ id, name, admin, %GameOptions{private: false} = options }) do
    Horde.Supervisor.start_child(
      Sketch.GlobalSupervisor,
      Sketch.GameServer.child_spec([id: id, name: name, admin: admin, options: options, on_events: [Sketch.GameRegistry]])
    )
  end

  def instantiate({ id, name, admin, %GameOptions{private: true} = options }) do
    Horde.Supervisor.start_child(
      Sketch.GlobalSupervisor,
      Sketch.GameServer.child_spec([id: id, name: name, admin: admin, options: options])
    )
  end

  def instantiate_public_game({ id, options }) do
    Horde.Supervisor.start_child(
      Sketch.GlobalSupervisor,
      Sketch.GameServer.child_spec([id: id, name: UUID.uuid4(:hex), options: options, on_events: [Sketch.Matchmaking]])
    )
  end


  def generate_game_id() do
    game_id = permalink(11)
    case Sketch.GameServer.exists?(game_id) do
      nil -> game_id
      _ -> generate_game_id()
    end
  end
end
