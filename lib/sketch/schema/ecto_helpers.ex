defmodule Sketch.EctoHelpers do
  import Ecto.Changeset
  # http://emailregex.com/
  @mail_regex ~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/

  @doc """
  Ensures that the email is valid
  """

  @spec validate_email(Ecto.Changeset.t(), :atom) :: Ecto.Changeset.t()
  def validate_email(changeset, field) do
    changeset |> validate_format(field, @mail_regex)
  end



  def error_map(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {_, value} ->
      Enum.into(value, %{}) |> filter_values
    end)
  end

  @spec error_status(%Ecto.Changeset{errors: list}) :: :atom
  def error_status(%Ecto.Changeset{errors: errors}) do
    Enum.reduce(errors, nil, fn {_, {_, error}}, _ -> error_status(Enum.into(error, %{})) end)
  end

  @spec error_status(%{validation: :required}) :: :bad_request
  def error_status(%{validation: :required}), do: :bad_request

  @spec error_status(%{validation: :length}) :: :bad_request
  def error_status(%{validation: :length}), do: :bad_request

  @spec error_status(%{validation: :format}) :: :bad_request
  def error_status(%{validation: :format}), do: :bad_request

  @spec error_status(%{validation: :unique}) :: :conflict
  def error_status(%{constraint: :unique}), do: :conflict

  defp filter_values(%{constraint: value}), do: %{constraint: value}
  defp filter_values(value), do: value
end
