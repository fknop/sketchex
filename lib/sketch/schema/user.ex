defmodule Sketch.User do
  @type t :: %__MODULE__{
    id: String.t(),
    email: String.t(),
    password: String.t(),
    username: String.t()
  }

  use Ecto.Schema

  import Ecto.Changeset
  import Sketch.EctoHelpers

  alias __MODULE__


  @primary_key {:id, Ecto.UUID, autogenerate: true}

  schema "users" do
    field :email, :string
    field :password, :string
    field :username, :string

    timestamps()
  end

  @spec changeset(User.t(), map) :: %Ecto.Changeset{}
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:username, :email, :password])
    |> validate_required([:username, :email, :password])
    |> validate_email(:email)
    |> validate_length(:username, min: 3)
    |> validate_length(:password, min: 5)
    |> unique_constraint(:email)
    |> put_hashed_password
  end

  @spec put_hashed_password(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp put_hashed_password(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}}
        ->
          put_change(changeset, :password, Bcrypt.hash_pwd_salt(password))
      _ ->
          changeset
    end
  end



  defmodule Query do
    import Ecto.Query

    alias Sketch.Repo
    alias Sketch.User

    @spec get_by_id(String.t()) :: {:ok, User.t()} | {:error, :not_found}
    def get_by_id(id) do

      query = from u in User,
        where: u.id == ^id,
        select: map(u, [:id, :username, :email])

      case Sketch.Repo.one(query) do
        nil ->
          {:error, :not_found}
        user ->
          {:ok, user}
      end
    end

    @spec get_by_email(String.t()) :: {:ok, User.t()} | {:error, :not_found}
    def get_by_email(email) do
      case Repo.get_by(User, email: email) do
        nil -> {:error, :not_found}
        user -> {:ok, user}
      end
    end

    @spec create_user(map) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
    def create_user(attrs \\ %{}) do
      %User{}
      |> User.changeset(attrs)
      |> Repo.insert()
    end
  end

end
