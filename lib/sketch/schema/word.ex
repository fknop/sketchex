defmodule Sketch.Word do
  use Ecto.Schema
  import Ecto.Changeset

  schema "words" do
    field :word, :string
    belongs_to :language_code, Sketch.Language, foreign_key: :language, references: :code, type: :string

    timestamps()
  end

  @doc false
  def changeset(word, attrs) do
    word
    |> cast(attrs, [:word])
    |> validate_required([:word])
    |> cast_assoc(:language, required: true)
  end


  defmodule Query do

    import Ecto.Query

    alias Sketch.Word
    alias Sketch.Language

    def get_random(language, n) do
      query =
        from w in Word,
        where: w.language == ^language,
        order_by: fragment("RANDOM()"),
        limit: ^n,
        select: w.word

      Sketch.Repo.all(query)
    end

  end
end
