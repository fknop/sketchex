defmodule Sketch.Language do
  use Ecto.Schema
  import Ecto.Changeset

  schema "languages" do
    field :name, :string
    field :code, :string

    has_many :words, Sketch.Word, foreign_key: :language, references: :code

    timestamps()
  end

  @doc false
  def changeset(language, attrs) do
    language
    |> cast(attrs, [:name, :code])
    |> validate_required([:name, :code])
  end


  defmodule Query do
    import Ecto.Query


  end
end
