defmodule Sketch.Error do
  use Ecto.Schema
  use SketchWeb, :params


  import Ecto.Changeset
  import Sketch.EctoHelpers

  schema "errors" do
    field :name, :string
    field :message, :string
    field :stack, :string
    field :language, :string 
    field :platform, :string 
    field :user_agent, :string

    timestamps()
  end

  @doc false
  def changeset(error, attrs) do
    error
    |> cast(attrs, [:name, :message, :stack, :language, :platform, :user_agent])
  end


  
  defmodule Query do
    import Ecto.Query
  
  end
  
end
