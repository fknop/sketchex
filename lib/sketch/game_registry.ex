defmodule Sketch.GameRegistry do
  use GenServer


  import Sketch.Crdt.Helpers

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def child_spec(opts) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  defmodule State do
    defstruct [:ets_table, :crdt]
  end

  def init(options) do
    crdt = Keyword.get(options, :crdt)
    :ets.new(__MODULE__, [:named_table, {:read_concurrency, true}])

    state = %State{
      ets_table: __MODULE__,
      crdt: crdt
    }

    {:ok, state}
  end


  def lookup(id) do
    GenServer.call(__MODULE__, {:lookup, id})
  end

  def select(spec) do
    GenServer.call(__MODULE__, {:select, spec})
  end

  def handle_call({:lookup, id}, _from, state) do
    {:reply, :ets.lookup(__MODULE__, id), state}
  end

  def handle_call({:select, spec}, _from, state) do
    {:reply, :ets.select(__MODULE__, spec), state}
  end

  def handle_info({:crdt_update, diffs}, state) do
    {:noreply, process_diffs(state, diffs, &process_diff/2)}
  end


  # Bugs because horde registry is not called from {:game, id}
  def handle_info({:game_server, :init, id, count, options}, state) do
    DeltaCrdt.mutate(state.crdt, :add, [id, {count, options}])
    update_browser_channel!()
    {:noreply, state}
  end

  def handle_info({:game_server, :join, id, count}, state) do
    [{^id, {_, options}}] = :ets.lookup(__MODULE__, id)
    DeltaCrdt.mutate(state.crdt, :add, [id, {count, options}])
    update_browser_channel!()
    {:noreply, state}
  end

  def handle_info({:game_server, :leave, id, count}, state) do
    [{^id, {_, options}}] = :ets.lookup(__MODULE__, id)
    DeltaCrdt.mutate(state.crdt, :add, [id, {count, options}])
    update_browser_channel!()
    {:noreply, state}
  end

  def handle_info({:game_server, :terminate, id, _reason}, state) do
    DeltaCrdt.mutate(state.crdt, :remove, [id])
    update_browser_channel!()
    {:noreply, state}
  end

  defp process_diff(state, {:add, id, value}) do
    :ets.insert(state.ets_table, {id, value})
    state
  end

  defp process_diff(state, {:remove, id}) do
    :ets.delete(state.ets_table, id)
    state
  end


  defp update_browser_channel!() do
    SketchWeb.Endpoint.broadcast!("public:browser", "update", %{})
  end
end
