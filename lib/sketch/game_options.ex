defmodule Sketch.GameOptions do
  use SketchWeb, :params
  import Ecto.Query, only: [from: 2]

  alias Sketch.Language

  @derive Jason.Encoder
  @primary_key false
  embedded_schema do
    field :name, :string
    field :turn_timeout, :integer, default: 90
    field :reveal_time, :integer, default: 15
    field :min_players, :integer, default: 2
    field :autostart, :boolean, default: false
    field :allow_join, :boolean, default: true
    field :private, :boolean, default: true
    field :language, :string, default: "en"
    field :max_round, :integer, default: 3
  end

  def changeset(struct, params \\ %{}) do
    # TODO: move this to somewhere else
    codes = Sketch.Repo.all(from l in Language, select: l.code)

    struct
    |> cast(params, [:name, :turn_timeout, :reveal_time, :min_players, :autostart, :allow_join, :private, :language, :max_round])
    |> validate_required(:name)
    |> validate_inclusion(:turn_timeout, [0, 10, 15, 20, 25, 30, 40, 50, 60, 90, 120])
    |> validate_inclusion(:reveal_time, [0, 15, 20, 30, 40])
    |> validate_inclusion(:max_round, [0, 3, 5, 7, 10])
    |> validate_number(:min_players, greater_than: 1, less_than: 11)
  end
end
