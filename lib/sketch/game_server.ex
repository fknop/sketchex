defmodule Sketch.GameServer do
  @moduledoc """

  User flow.


  Event: :new_round

    --> select_drawer
    --> send "word_suggestions" to drawer
    --> schedule timeout :words_timeout

    --> if drawer sends word or :words_timeout
      --> select word and emit "start_draw" with the size of the words and places of spaces

    --> schedule :time_ellapsed
    --> receive "chat" event
      --> if everyone except drawer found the word
        --> :over event
      --> if :time_ellapsed
        --> :over event

    --> :over event
      compute scores for every players that found the word
      and for the drawer

      schedule :new_round after @between_round
  """


  use GenServer, restart: :transient, shutdown: 10_000

  alias Sketch.GameOptions
  alias Sketch.PlayerSession

  require Logger


  @type event :: list(number)
  @type initial_state :: %{
    player_id: String.t(),
    events: list(event),
    admin: boolean(),
    word: String.t() | nil,
    characters: list(String.t()),
    drawer: String.t() | nil,
    players: list(map),
    timer: map,
    suggestions: list(String.t()) | nil,
    options: %GameOptions{},
    round: integer(),
    node: String.t()
  }

  @timeout 5 * 60 * 1_000  # 5 minutes
  @between_round 5         # 5 seconds


  # Channels messages
  @server_info "server_info"

  @game_timer_event "game_timer"
  @round_start_event "round_start"
  @draw_event "draw"
  @fill_event "fill"
  @undo_event "undo"
  @redo_event "redo"
  @chat_event "chat"
  @pass_event "pass"
  @clear_event "clear"
  @reveal_event "reveal"
  @word_found_event "word_found"
  @start_draw_event "start_draw"
  @over_event "over"
  @user_join_event "user_join"
  @user_leave_event "user_leave"
  @wait_event "wait"
  @game_over_event "game_over"

  @start_migration "start_migration"
  @end_migration "end_migration"

  @word_choice_event "draw:word_choice"
  @word_suggestions_event "draw:word_suggestions"
  @colors MapSet.new(Sketch.RGBConstants.colors())




  def start_link(opts) do
    id = Keyword.get(opts, :id)
    GenServer.start_link(__MODULE__, opts, name: via_tuple(id))
  end

  def terminate(:normal, state) do
    Logger.info("**** Game process #{state.game_id} timed out.")
    Sketch.Handoff.unrequest(state.game_id)

    for listener <- state.listeners do
      send(listener, {:game_server, :terminate, state.game_id, :normal})
    end

    broadcast_lobby!(state, @server_info, %{type: :stop, reason: :timeout})
    :ok
  end

  def terminate(reason, state) do
    Logger.info("**** Game process #{state.game_id} stopped due to #{inspect(reason)}")

    do_handoff(state)

    for listener <- state.listeners do
      send(listener, {:game_server, :terminate, state.game_id, reason})
    end

    :ok
  end

  defp do_handoff(state) do

    broadcast_lobby!(state, @start_migration)

    Sketch.Handoff.unrequest(state.game_id)

    turn_timer = state.turn_timer
    turn_timer_value = case turn_timer do
      nil -> nil
      ref -> Process.read_timer(ref)
    end

    reveal_timer = state.reveal_timer
    reveal_timer_value = case reveal_timer do
      nil -> nil
      ref -> Process.read_timer(ref)
    end

    handoff_state =
      state
      |> Map.delete(:turn_timer)
      |> Map.delete(:reveal_timer)
      |> Map.put(:turn_timer_value, turn_timer_value)
      |> Map.put(:reveal_timer_value, reveal_timer_value)

    Sketch.Handoff.handoff(handoff_state.game_id, handoff_state)
    Process.sleep(100) # Let time for the DeltaCrdt to propagate
  end


  defp do_up(handoff_state) do
    %{
      reveal_timer_value: reveal_timer_value,
      turn_timer_value: turn_timer_value
    } = handoff_state


    # Reset reveal timer with value from stopped server
    reveal_timer = case reveal_timer_value do
      nil -> nil
      false -> nil
      time -> Process.send_after(self(), :reveal, time)
    end

    # Reset turn timer with value from stopped server
    turn_timer = case turn_timer_value do
      nil -> nil
      false -> nil
      time ->
        case handoff_state.word do
          nil -> Process.send_after(self(), :words_timeout, time)
          _ -> Process.send_after(self(), {:time_ellapsed, handoff_state.drawer}, time)
        end
    end

    # Schedule new round if server stopped during rounds
    if handoff_state.over do
      schedule_new_round(@between_round)
    end

    handoff_state
    |> Map.delete(:reveal_timer_value)
    |> Map.delete(:turn_timer_value)
    |> Map.put(:turn_timer, turn_timer)
    |> Map.put(:reveal_timer, reveal_timer)
  end


  def via_tuple(name) do
    {:via, Horde.Registry, {Sketch.GlobalRegistry, {:game, name}}}
  end

  def exists?(name) do
    name
      |> via_tuple
      |> GenServer.whereis
  end

  def init(opts) do

    id = Keyword.get(opts, :id)
    name = Keyword.get(opts, :name)
    admin = Keyword.get(opts, :admin, nil)
    options = Keyword.get(opts, :options, %GameOptions{})
    listeners = Keyword.get(opts, :on_events, [])

    Logger.info("Creating game server #{id} on node #{inspect Node.self()}")

    Process.flag(:trap_exit, true)

    state = %{
      game_id: id,
      name: name,
      admin: admin,
      options: options,
      players: [],
      events: [],
      undo_checkpoints: [0],
      redo: [],
      drawer: nil,
      first_drawer: nil,
      word: nil,
      words: [],
      characters: nil,
      turn_timer: nil,
      reveal_timer: nil,
      found_order: 0,
      round: 0,
      over: false,
      listeners: listeners
    }

    {:ok, state, {:continue, :after_init}}
  end

  @spec draw_event(String.t(), String.t(), event) :: :ok
  def draw_event(name, user_id, event) do
    name
    |> via_tuple
    |> GenServer.call({ :draw_event, user_id, event })
  end

  @spec fill_event(String.t(), String.t(), event) :: :ok
  def fill_event(name, user_id, event) do
    name
    |> via_tuple
    |> GenServer.call({ :fill_event, user_id, event })
  end

  @spec get_events(String.t()) :: list(event)
  def get_events(name) do
    name
    |> via_tuple
    |> GenServer.call({ :get_events })
  end

  @spec get_initial_state(String.t(), String.t()) :: initial_state
  def get_initial_state(name, user_id) do
    name
    |> via_tuple
    |> GenServer.call({ :get_initial_state, user_id })
  end

  @spec get_options(String.t()) :: %GameOptions{}
  def get_options(name) do
    name
    |> via_tuple
    |> GenServer.call({ :get_options })
  end

  @spec undo(String.t(), String.t()) :: :ok
  def undo(name, user_id) do
    name
    |> via_tuple
    |> GenServer.call({ :undo, user_id })
  end

  @spec redo(String.t(), String.t()) :: :ok
  def redo(name, user_id) do
    name
    |> via_tuple
    |> GenServer.call({ :redo, user_id })
  end

  @spec checkpoint(String.t(), String.t()) :: :ok
  def checkpoint(name, user_id) do
    name
    |> via_tuple
    |> GenServer.call({ :checkpoint, user_id})
  end

  @spec clear(String.t(), String.t()) :: :ok
  def clear(name, user_id) do
    name
    |> via_tuple
    |> GenServer.call({ :clear, user_id })
  end

  def join(name, player) do
    name
    |> via_tuple
    |> GenServer.call({ :join, player })
  end

  def leave(name, player) do
    name
    |> via_tuple
    |> GenServer.call({:leave, player})
  end

  def start_game(name, user_id) do
    name
    |> via_tuple
    |> GenServer.call({ :start, user_id })
  end

  def chat_message(name, from, message) do
    name
    |> via_tuple
    |> GenServer.call({ :chat, from, message })
  end

  def pass(name, user_id) do
    name
    |> via_tuple
    |> GenServer.call({ :pass, user_id })
  end

  def choose_word(name, user_id, word) do
    name
    |> via_tuple
    |> GenServer.call({ :choose_word, user_id, word })
  end


  # Handle choose_word when user_id is the drawer
  def handle_call({ :choose_word, user_id, word }, _from, state = %{ drawer: drawer }) when drawer == user_id do
    case Enum.find(state.words, &(&1 == word)) do
      nil ->
        {:reply, :error, state, @timeout}
      _   ->
        {:reply, :ok, set_word(state, word), {:continue, :start_drawing}}
    end
  end

  # Handle choose_word when user_id is not the drawer
  def handle_call({ :choose_word, _, _ }, _from, state), do:  {:reply, :error, state, @timeout}

  def handle_call({ :draw_event, user_id, event }, _from, %{drawer: drawer} = state) when user_id == drawer do
    broadcast_lobby!(state, @draw_event, %{event: event})
    {:reply, :ok, %{state | events: [[0 | event] | state.events], redo: []}, @timeout}
  end

  # user_id is not the current player
  def handle_call({ :draw_event, _, _ }, _from, state) do
    {:reply, :ok, state, @timeout}
  end

  def handle_call({ :fill_event, user_id, event }, _from, %{drawer: drawer} = state) when user_id == drawer do
    broadcast_lobby!(state, @fill_event, %{event: event})
    {:reply, :ok, %{state | events: [[1 | event] | state.events], redo: []}, {:continue, :checkpoint}}
  end

  def handle_call({ :fill_event, _, _ }, _from, state) do
    {:reply, :ok, state, @timeout}
  end


  def handle_call({ :undo, _ }, _from, %{events: []} = state), do: {:reply, :error, state, @timeout}

  def handle_call({ :undo, user_id }, _from, %{drawer: drawer} = state) when user_id == drawer do
    [h | _] = state.undo_checkpoints
    count = Enum.count(state.events)

    [checkpoint | checkpoints] = if h == count do
      case state.undo_checkpoints do
        [_, checkpoint | []] -> [checkpoint | [0]]
        [_, checkpoint | checkpoints] -> [checkpoint | [checkpoint | checkpoints]]
      end
    else
      case state.undo_checkpoints do
        [checkpoint | []] -> [checkpoint | [0]]
        [_checkpoint | _checkpoints] -> state.undo_checkpoints
      end
    end

    {events, redo} = if checkpoint == 0, do: {[], Enum.reverse(state.events)}, else: remove_first(state.events, count - checkpoint, [])
    broadcast_lobby!(state, @undo_event, %{events: events})

    {:reply, :ok, %{state | events: events, undo_checkpoints: checkpoints, redo: [redo | state.redo]}, @timeout}
  end

  def handle_call({:undo, _}, _from, state), do: {:reply, :error, state, @timeout}

  def handle_call({:redo, _}, _from, %{redo: []} = state), do: {:reply, :error, state, @timeout}

  def handle_call({ :redo, user_id }, _from, %{drawer: drawer} = state) when user_id == drawer do
    [events | remaining_redos] = state.redo
    broadcast_lobby!(state, @redo_event, %{events: events})
    {:reply, :ok, %{state | events: prepend_reversed(state.events, events), redo: remaining_redos}, {:continue, :checkpoint}}
  end

  def handle_call({:redo, _}, _from, state), do: {:reply, :error, state, @timeout}

  def handle_call({ :get_events }, _from, state) do
    {:reply, state.events, state, @timeout}
  end

  def handle_call({:checkpoint, _}, _from, %{events: []} = state), do: {:reply, :error, state, @timeout}
  def handle_call({:checkpoint, user_id}, _from, %{drawer: drawer} = state) when user_id == drawer do
    {:reply, :ok, state, {:continue, :checkpoint}}
  end

  def handle_call({:checkpoint, _}, _from, state), do: {:reply, :error, state, @timeout}

  def handle_call({ :get_initial_state, user_id }, _from, state) do
    case Enum.find(state.players, &(&1.user_id == user_id)) do
      nil ->
        {:reply, nil, state}
      player ->

        time =
        with ref when not is_nil(ref) <- state.turn_timer,
              value when is_number(value) <- Process.read_timer(ref)
        do
          value
        else _ ->
          0
        end

        {start_time, end_time} = compute_start_end(time)

        initial_state = %{
          player_id: player.id,
          events: state.events,
          admin: is_admin?(state, user_id),
          word: (if state.drawer == user_id, do: state.word, else: nil),
          characters: state.characters,
          drawer: get_drawer_id(state),
          players: state.players |> remove_user_ids,
          timer: %{start_time: start_time, end_time: end_time, time: time},
          suggestions: (if state.drawer == user_id, do: state.words, else: nil),
          options: state.options,
          round: state.round,
          node: Node.self()
        }

        {:reply, initial_state, state, @timeout}
    end
  end



  def handle_call({ :get_options }, _from, state) do
    {:reply, state.options, state, @timeout}
  end

  def handle_call({ :clear, user_id }, _from, %{drawer: drawer} = state) when drawer == user_id do
    broadcast_lobby!(state, @clear_event, %{})
    {:reply, :ok, %{state | events: [], undo_checkpoints: [0], redo: []}, @timeout}
  end

  def handle_call({ :clear, _ }, _from, state), do: {:reply, :ok, state, @timeout}


  def handle_call({ :start, user_id }, _from, state) do
    cond do
      not can_start?(state, user_id) -> {:reply, {:error, :unauthorized}, state, @timeout}
      not enough_players?(state)     -> {:reply, {:error, :missing_players}, state, @timeout}
      true ->                           {:reply, :ok, state, {:continue, :new_round}}
    end
  end


  def handle_call({ :join, player }, _from, state = %{ players: players }) do
    cond do
      Enum.any?(players, &( Map.get(&1, :user_id) == player.user_id )) ->
        {:reply, {:error, :already_seated}, state, @timeout}

      Enum.count(players) >= 10 ->
        {:reply, {:error, :server_full}, state, @timeout}

      started?(state) and not allow_join?(state) ->
        {:reply, {:error, :cannot_join_in_progress}, state, @timeout}

      true ->
        admin = if is_nil(state.admin), do: player.user_id, else: state.admin
        player = new_player(state, player)

        {
          :reply,
          {:ok, player.id},
          %{ state | players: [player | players], admin: admin },
          {:continue, :join}
        }
    end
  end


  def handle_call({ :leave, user_id }, _from, state) do
    case find_player_index(state, user_id) do
      nil ->
        {:reply, {:error, :unknown_player}, state, @timeout}
      index ->
        player = Enum.at(state.players, index)
        {:reply, :ok, do_leave(state, index), {:continue, {:leave, player}}}
    end
  end

  def handle_call({ :chat, from, message }, _from, state) do

    unless message == state.word && from != state.drawer && !state.over do

      if message != state.word do
        broadcast_lobby!(state, @chat_event, %{"message" => message, "from" => message_from(state, from)})
      end

      {:reply, :ok, state, @timeout}
    else
      index = find_player_index(state, from)
      player = Enum.at(state.players, index)

      if player.found or player.pass do
        {:reply, :ok, state, @timeout}
      else
        player = player
        |> Map.put(:found, true)
        |> Map.put(:found_order, state.found_order + 1)

        broadcast_lobby!(state, @word_found_event, %{ player: remove_user_id(player) })

        {
          :reply,
          :ok,
          state
          |> put_in([:players, Access.at(index)], player)
          |> Map.put(:found_order, state.found_order + 1),
          {:continue, :found}
        }
      end
    end
  end

  def handle_call({ :pass, user_id }, _from, %{drawer: drawer, over: over, word: word} = state)
    when word != nil and drawer != nil and user_id != drawer and not over do

      index = find_player_index(state, user_id)
      player = Enum.at(state.players, index)

      if player.pass or player.found do
        {:reply, :ok, state, @timeout}
      else
        player = Map.put(player, :pass, true)
        broadcast_lobby!(state, @pass_event, %{ player: remove_user_id(player) })

        {
          :reply,
          :ok,
          state |> put_in([:players, Access.at(index)], player),
          {:continue, :found}
        }
      end
  end

  def handle_call({ :pass, _ }, _from, state), do: {:reply, :ok, state, @timeout}

  def handle_continue(:after_init, state) do

    {migrate, state} = case Sketch.Handoff.request(state.game_id, self(), :requested_handoff) do
      {:ok, :requested} ->
        {false, state}
      {:ok, data} ->
        {true, do_up(data)}
    end

    for listener <- state.listeners do
      send(listener, {:game_server, :init, state.game_id, Enum.count(state.players), state.options})
    end

    if migrate do
      {:noreply, state, {:continue, :after_migrate}}
    else
      {:noreply, state, @timeout}
    end
  end

  def handle_continue(:after_migrate, state) do
    Logger.info("Rebooted game server: #{inspect state.game_id}")

    time =
      with ref when not is_nil(ref) <- state.turn_timer,
           value when is_number(value) <- Process.read_timer(ref)
      do
        value
      else _ ->
        0
      end

    {start_time, end_time} = compute_start_end(time)

    payload = %{
      events: state.events,
      characters: state.characters,
      drawer: get_drawer_id(state),
      players: state.players |> remove_user_ids,
      timer: %{start_time: start_time, end_time: end_time, time: time},
      node: Node.self()
    }

    broadcast_lobby!(state, @end_migration, payload)
    {:noreply, state, @timeout}
  end

  def handle_continue(:found, state) do
    state.players
    |> Enum.filter(&(&1.user_id != state.drawer))
    |> Enum.filter(&(not &1.pass))
    |> Enum.all?(&(&1.found))
    |> if do
      {:noreply, state, {:continue, :over}}
    else
      {:noreply, state, @timeout}
    end
  end

  def handle_continue(:start_drawing, state) do
    %{drawer: drawer, word: word} = state
    broadcast_to_user!(state, drawer, @word_choice_event, %{ word: word })
    broadcast_lobby!(state, @start_draw_event, %{ characters: state.characters })
    {:noreply, state, @timeout}
  end

  def handle_continue(:reveal, state) do
    broadcast_lobby!(state, @reveal_event, %{ characters: state.characters })
    {:noreply, state, @timeout}
  end

  def handle_continue(:over, state) do
    cancel_time_left(state)
    cancel_reveal(state)

    player_scores = state.players |> Enum.map(&compute_score(state, &1))
    scores = Enum.map(player_scores, &(elem(&1, 0))) |> Enum.map(fn {id, score} -> %{id: id, score: score} end)
    players = Enum.map(player_scores, &(elem(&1, 1)))


    broadcast_lobby!(state, @over_event, %{ word: state.word, players: remove_user_ids(players), scores: scores })

    schedule_new_round(@between_round)
    {:noreply, %{state | players: players, over: true}, @timeout}
  end


  def handle_continue(:checkpoint, state) do
    count = Enum.count(state.events)
    {:noreply, %{state | undo_checkpoints: [count | state.undo_checkpoints]}, @timeout}
  end


  def handle_continue(:new_round, state) do
    broadcast_lobby!(state, @clear_event, %{})
    case state |> reset_round |> new_round do
      {:ok, state} -> {:noreply, state, {:continue, { :play, :start }}}
      {:waiting, state} -> {:noreply, state |> reset_server, {:continue, :broadcast_wait }}
      {:over, state} -> {:noreply, state, {:continue, :broadcast_over}}
    end
  end


  def handle_continue({:play, :start}, state) do
    cancel_time_left(state)
    {:noreply, schedule_words_timer(state), {:continue, :broadcast_start}}
  end

  def handle_continue(:broadcast_start, state) do
    broadcast_lobby!(state, @round_start_event, %{
      drawer: get_drawer_id(state),
      players: remove_user_ids(state.players),
      round: state.round
    })

    {:noreply, state, @timeout}
  end

  def handle_continue(:broadcast_wait, state) do
    broadcast_lobby!(state, @wait_event)
    {:noreply, state, @timeout}
  end



  def handle_continue(:broadcast_over, state) do
    broadcast_lobby!(state, @game_over_event)
    {start_time, end_time} = compute_start_end(20 * 1_000)
    broadcast_lobby!(state, @game_timer_event, %{ event: :word_timer, start_time: start_time, end_time: end_time, time: 20 * 1000 })

    Process.send_after(self(), :server_close, 20000)
    {:noreply, state, @timeout}
  end

  def handle_info(:server_close, state) do
    {:stop, :normal, state}
  end


  def handle_continue(:join, %{players: [player | _] = players} = state) do

    PlayerSession.join_game(player.user_id, state.game_id)

    for listener <- state.listeners do
      send(listener, {:game_server, :join, state.game_id, Enum.count(state.players)})
    end


    Logger.debug(inspect {:join, state.admin, players})

    broadcast_lobby!(state, @user_join_event, %{
      player: player |> remove_user_id,
      players: players |> remove_user_ids,
      admin: get_admin_id(state)
    })

    if not started?(state) and autostart?(state) and enough_players?(state) do
      {:noreply, state, {:continue, :new_round}}
    else
      {:noreply, state, @timeout}
    end
  end

  def handle_continue({ :leave, _ }, %{ players: players } = state) when length(players) == 0 do
    Logger.debug(inspect {:leave, state.admin})
    {:stop, :normal, state}
  end


  def handle_continue({ :leave, player }, %{ players: players } = state) when length(players) == 1 do
    {:noreply, broadcast_leave(state, player), {:continue, :over}}
  end

  def handle_continue({ :leave, player = %{user_id: drawer} }, %{ drawer: drawer } = state) do
    {:noreply, broadcast_leave(state, player), {:continue, :over}}
  end

  def handle_continue({ :leave, player }, state) do
    {:noreply, broadcast_leave(state, player), @timeout}
  end

  def handle_info({:requested_handoff, data}, _state) do
    {:noreply, do_up(data), {:continue, :after_migrate}}
  end

  def handle_info(:timeout, state) do
    {:stop, :normal, state}
  end

  def handle_info({:time_ellapsed, _player}, state) do
    {:noreply, state, {:continue, :over}}
  end

  def handle_info(:words_timeout, state) do
    %{words: words} = state
    word = Enum.random(words)
    {:noreply, set_word(state, word), {:continue, :start_drawing}}
  end

  def handle_info(:reveal, %{ characters: nil } = state) do
    {:noreply, %{state | reveal_timer: nil}, @timeout}
  end

  def handle_info(:reveal, state) do
    {:noreply, %{state | characters: assign_random_char(state)} |> schedule_reveal, {:continue, :reveal}}
  end

  def handle_info(:new_round, state) do
    {:noreply, state, {:continue, :new_round}}
  end

  def handle_info({:EXIT, _pid, reason}, state) do
    {:stop, reason, state}
  end

  defp compute_score(%{drawer: drawer} = state, %{user_id: drawer} = player) do
    count = Enum.count(state.players, &(&1.found))
    score = player.score
    increment = (150 * count)
    {{player.id, increment}, Map.put(player, :score, score + increment)}
  end

  defp compute_score(_state, player) do
    %{found_order: found_order, found: found, score: score} = player
    if found do
      increment = 1500 - (found_order * 100)
      {{player.id, increment}, Map.put(player, :score, score + increment)}
    else
      {{player.id, 0}, player}
    end
  end


  defp schedule_reveal(%{options: %GameOptions{reveal_time: reveal_time}} = state) when reveal_time > 0 do
    timer = Process.send_after(self(), :reveal, reveal_time * 1000)
    %{state | reveal_timer: timer}
  end

  defp schedule_reveal(state), do: state

  defp set_word(state, word) do
    cancel_time_left(state)
    %{ state | word: word, words: [], characters: transform_word(word) } |> schedule_time_left |> schedule_reveal
  end

  defp assign_random_char(%{ characters: characters, word: word }) do
    with indexed_chars = Enum.with_index(characters),
         filtered_chars = Enum.filter(indexed_chars, fn {c, _} -> c == "_" end),
         {:empty?, false} <- {:empty?, Enum.empty?(filtered_chars)} do
      index = Enum.random(filtered_chars) |> elem(1)
      char = String.at(word, index)

      List.replace_at(characters, index, char)
    else
      {:empty?, true} -> characters
    end
  end

  defp do_leave(state, index) do

    players = state.players
    player = Enum.at(players, index)
    drawer = state.drawer
    first_drawer = state.first_drawer
    count = Enum.count(players)


    new_first_drawer = cond do
      drawer == first_drawer && count > 2 ->
        first_drawer_index = rem(index + 1, count)
        Enum.at(players, first_drawer_index)
      drawer == first_drawer && count <= 2 ->
        nil
      true ->
        first_drawer
    end

    state
    |> Map.put(:players, List.delete_at(state.players, index))
    |> Map.put(:admin, set_new_admin(state, player.user_id))
    |> Map.put(:first_drawer, new_first_drawer)
  end

  defp set_new_admin(%{ admin: user_id, players: [] }, user_id), do: nil
  defp set_new_admin(%{ admin: user_id, players: [%{user_id: admin} | _] }, user_id), do: admin
  defp set_new_admin(%{ admin: admin }, _user_id), do: admin

  defp schedule_new_round(t) do
    Process.send_after(self(), :new_round, t * 1000)
  end

  defp compute_start_end(timeout) do
    current_time = :os.system_time(:millisecond)
    end_time = current_time + timeout
    {current_time, end_time}
  end

  defp schedule_words_timer(state) do
    cancel_time_left(state)
    timeout = 15
    {start_time, end_time} = compute_start_end(timeout * 1_000)
    broadcast_lobby!(state, @game_timer_event, %{ event: :word_timer, start_time: start_time, end_time: end_time, time: timeout * 1000 })
    timer = Process.send_after(self(), :words_timeout, timeout * 1_000)
    %{state | turn_timer: timer}
  end




  defp schedule_time_left(state = %{ options: %GameOptions{ turn_timeout: 0 } }), do: state
  defp schedule_time_left(state) do
    cancel_time_left(state)
    timeout = state.options.turn_timeout

    {start_time, end_time} = compute_start_end(timeout * 1_000)
    broadcast_lobby!(state, @game_timer_event, %{ event: :play, start_time: start_time, end_time: end_time, time: timeout * 1000 })
    timer = Process.send_after(self(), {:time_ellapsed, state.drawer}, timeout * 1_000)
    %{state | turn_timer: timer}
  end

  defp cancel_time_left(%{turn_timer: nil}), do: nil
  defp cancel_time_left(%{turn_timer: timer}), do: Process.cancel_timer(timer)

  defp cancel_reveal(%{reveal_timer: nil}), do: nil
  defp cancel_reveal(%{reveal_timer: timer}), do: Process.cancel_timer(timer)


  defp broadcast_leave(state, player) do

    PlayerSession.leave_game(player.user_id, state.game_id)

    for listener <- state.listeners do
      send(listener, {:game_server, :leave, state.game_id, Enum.count(state.players)})
    end

    broadcast_lobby!(state, @user_leave_event, %{
      player: player |> remove_user_id,
      players: state.players |> remove_user_ids,
      admin: get_admin_id(state)
    })

    state
  end




  defp new_round(state) do
    Logger.debug(inspect {:new_round, state.game_id, state.drawer})
    %{min_players: min} = state.options

    state = state |> set_current_player |> set_first_drawer |> advance_round

    cond do
      state.options.max_round > 0 && state.round > state.options.max_round ->
        # TODO: winner
        {:over, state}
      Enum.count(state.players) >= min ->
        {:ok, state |> generate_words |> broadcast_words}
      true ->
        {:waiting, state}
    end
  end

  defp set_first_drawer(%{drawer: drawer, first_drawer: nil} = state), do: %{state | first_drawer: drawer}
  defp set_first_drawer(state), do: state
  defp advance_round(%{first_drawer: drawer, drawer: drawer} = state), do: %{state | round: state.round + 1 }
  defp advance_round(state), do: state

  defp reset_round(state) do
    %{state |
      words: [],
      word: nil,
      characters: nil,
      turn_timer: nil,
      reveal_timer: nil,
      players: state.players |> Enum.map(&reset_player/1),
      events: [],
      undo_checkpoints: [0],
      redo: [],
      found_order: 0,
      over: false
    }
  end

  defp reset_server(state) do
    state
      |> reset_round
      |> Map.put(:drawer, nil)
      |> Map.put(:players,
        state.players |> Enum.map(&(Map.put(&1, :score, 0)))
      )
      |> Map.put(:first_drawer, nil)
      |> Map.put(:round, 0)
  end

  @spec new_player_id(map) :: integer()
  defp new_player_id(%{ players: [] }), do: 0
  defp new_player_id(%{ players: [%{id: previous_id} | _] }), do: previous_id + 1


  @spec new_player(map, map) :: map
  defp new_player(state, player) do
    player
    |> Map.put(:id, new_player_id(state))
    |> Map.put(:score, 0)
    |> Map.put(:color, assign_color(state))
    |> reset_player
  end

  @spec reset_player(map) :: map
  defp reset_player(player) do
    player
    |> Map.put(:found, false)
    |> Map.put(:found_order, 0)
    |> Map.put(:pass, false)
  end


  defp generate_words(state) do
    %{state | words: get_random_words(state.options.language)}
  end

  defp get_random_words(language) do
    Sketch.Word.Query.get_random(language, 3)
  end

  defp broadcast_words(state) do
    %{drawer: drawer, words: words} = state
    broadcast_to_user!(state, drawer, @word_suggestions_event, %{words: words})
    state
  end


  defp transform_word(nil), do: nil
  defp transform_word(word) do
    word
    |> String.codepoints
    |> Enum.map(&(if &1 == " ", do: &1, else: "_"))
  end


  @spec assign_color(map) :: String.t()
  defp assign_color(state) do
    used_colors = state.players |> Enum.map(&(&1.color)) |> MapSet.new
    unused_colors = MapSet.difference(@colors, used_colors)
    Enum.at(unused_colors, 0)
  end

  defp set_current_player(state) do
    new = case find_player_index(state, state.drawer) do
      nil -> 0
      current  ->
        count = Enum.count(state.players)
        rem(current + 1, count)
    end

    %{state | drawer: Enum.at(state.players, new).user_id}
  end

  defp get_drawer_id(state) do
    case Enum.find(state.players, &(&1.user_id == state.drawer)) do
      nil -> nil
      %{id: id} -> id
    end
  end

  defp is_admin?(%{admin: admin}, user_id), do: admin == user_id

  defp can_start?(%{admin: nil}, _), do: true
  defp can_start?(%{admin: admin}, user_id), do: user_id == admin


  defp autostart?(%{options: %GameOptions{ autostart: autostart }}), do: autostart
  defp allow_join?(%{options: %GameOptions{ allow_join: allow_join }}), do: allow_join
  defp started?(%{drawer: nil}), do: false
  defp started?(_state), do: true
  defp enough_players?(%{ options: %GameOptions{min_players: min}, players: players }), do: Enum.count(players) >= min


  defp get_admin_id(%{admin: nil}), do: nil
  defp get_admin_id(state) do
    case Enum.find(state.players, &(&1.user_id == state.admin)) do
      nil -> nil
      player -> player.id
    end
  end


  defp find_player_index(%{players: players}, user_id) do
     Enum.find_index(players, &(&1.user_id == user_id))
  end

  defp remove_user_ids(players), do: Enum.map(players, &remove_user_id/1)
  defp remove_user_id(nil), do: nil
  defp remove_user_id(player), do: Map.delete(player, :user_id)


  defp message_from(state, from) do
    state.players |> Enum.find(&(&1.user_id == from)) |> remove_user_id
  end

  defp user_payload(state, payload), do: Map.put(payload, :game_id, state.game_id)

  defp broadcast_to_user!(state, user_id, message, payload) do
    SketchWeb.Endpoint.broadcast("user:" <> user_id, message, user_payload(state, payload))
  end

  defp broadcast_lobby!(%{ game_id: game_id }, message, payload \\ %{}) do
    SketchWeb.Endpoint.broadcast!("draw:" <> game_id, message, payload)
  end


  defp remove_first([], _, removed), do: {[], removed}
  defp remove_first(list, 0, removed), do: {list, removed}
  defp remove_first([h | t], count, removed) do
    remove_first(t, count - 1, [h | removed])
  end

  defp prepend_reversed(list, []), do: list
  defp prepend_reversed(list, [h | t]), do: prepend_reversed([h | list], t)
end
