defmodule Sketch.Macros do
  defmacro dev_mode(do: block) do
    if Mix.env == :dev do
      block
    end
  end
end
