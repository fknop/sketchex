defmodule Sketch.Helpers.Pagination do
  

  def paginate_list(list, page, limit, mapper \\ nil) do
    count = Enum.count(list)
    page_count = div(count, limit) + Kernel.min(rem(count, limit), 1)
    has_next = count > 0 && page < page_count 
    has_previous = count > 0 && page > 1
    
    offset = (page - 1) * limit

    results = list 
    |> Enum.drop(offset)
    |> Enum.take(limit)
    |> map_if_mapper(mapper)
  
    metadata = %{
      total_count: count,
      count: Enum.count(results),
      page_count: page_count,
      has_next: has_next,
      has_previous: has_previous
    }

    {results, metadata}
  end


  defp map_if_mapper(list, mapper) do
    case mapper do
      nil -> list 
      _   -> list |> Enum.map(mapper)
    end
  end
end