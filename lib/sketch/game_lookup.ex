defmodule Sketch.GameLookup do

  import Sketch.Helpers.Pagination
  require Logger

  # @public_games [{
  #   {:"$1", :"$2", :"$3"},
  #   [{:==, {:element, 1, :"$1"}, :game}, {:"=/=", :"$3", nil}],
  #   [{{ {:element, 2, :"$1" }, :"$3"}}]
  # }]

  @public_games [{
    {:"$1", :"$2"},
    [],
    [{{ :"$1", :"$2"}}]
  }]


  def get(id) do
    case Sketch.GameRegistry.lookup(id) do
      [] -> nil
      [{^id, _count, options}] -> options
    end
  end

  def list(page, limit) do

    games = Sketch.GameRegistry.select(@public_games)
    # games = Horde.Registry.select(Sketch.GlobalRegistry, @public_games)

    {results, metadata} = paginate_list(
      games,
      page,
      limit,
      fn {key, {slots, options}} ->
        %{
          id: key,
          options: options,
          slots: slots
        }
      end
    )

    %{ results: results, metadata: metadata }
  end
end
