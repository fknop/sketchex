defmodule Sketch.Crdt.Helpers do


  def process_diffs(state, [], _), do: state
  def process_diffs(state, [diff | diffs], process) do
    process_diff(state, diff, process) |> process_diffs(diffs, process)
  end

  defp process_diff(state, {:add, id, value}, process) do
    # :ets.insert(state.ets_table, {id, value})
    process.(state, {:add, id, value})
  end

  defp process_diff(state, {:remove, id}, process) do
    # :ets.delete(state.ets_table, id)
    process.(state, {:remove, id})
  end
end
