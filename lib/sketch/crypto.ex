defmodule Sketch.Crypto do
  def permalink(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64 |> binary_part(0, length)
  end

  def player_uuid, do: UUID.uuid4(:hex)
end
