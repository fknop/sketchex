defmodule Sketch.PlayerSession do
  use GenServer, restart: :transient

  require Logger


  alias Sketch.GameServer

  @rejoin_timeout 10 * 1_000 # 10s
  @guest_chips 1000

  def start_link({ user_id, :guest }) do
    GenServer.start_link(__MODULE__, %{user_id: user_id, guest: true, chips: @guest_chips, server: nil }, name: via_tuple(user_id))
  end

  def start_link({ user_id, :user }) do
    GenServer.start_link(__MODULE__, %{user_id: user_id, guest: false, server: nil })
  end

  def via_tuple(user_id) do
    {:via, Horde.Registry, {Sketch.GlobalRegistry, {:session, user_id}}}
  end

  def exists?(user_id) do
    via_tuple(user_id) |> GenServer.whereis
  end

  def stop(user_id) do
    via_tuple(user_id) |> GenServer.stop(:normal)
  end

  def start_away(user_id) do
    tuple = via_tuple(user_id)
    case GenServer.whereis(tuple) do
      nil -> nil
      _ -> GenServer.call(tuple, :start_away)
    end
  end

  def stop_away(user_id) do
    tuple = via_tuple(user_id)
    case GenServer.whereis(tuple) do
      nil -> nil
      _ -> GenServer.call(tuple, :stop_away)
    end
  end

  def join_game(user_id, game_id) do
    via_tuple(user_id) |> GenServer.cast({:joined, game_id})
  end

  def leave_game(user_id, game_id) do
    via_tuple(user_id) |> GenServer.cast({:left, game_id})
  end

  def init(state) do
    {:ok, state |> Map.put(:timer, nil)}
  end

  def terminate(reason, _state) do
    Logger.debug("Player session terminated")
    Logger.debug(inspect reason)
  end

  def handle_call(:start_away, _from, state = %{timer: nil}) do
    timer = Process.send_after(self(), :away, @rejoin_timeout)
    {:reply, :ok, %{state | timer: timer}}
  end

  def handle_call(:start_away, _from, state = %{timer: _}), do: {:reply, :ok, state}

  def handle_call(:stop_away, _from, state = %{timer: nil}), do: {:reply, :ok, state}

  def handle_call(:stop_away, _from, state = %{timer: timer}) do
    Process.cancel_timer(timer)
    {:reply, :ok, %{state | timer: nil}}
  end


  def handle_cast({:joined, game_id}, %{server: server} = state) do
    case GameServer.exists?(game_id) do
      nil -> {:noreply, state}
      _ ->
        case server do
          nil ->
            {:noreply, %{state | server: game_id}}
          _ ->
            # Only allow one game at a time
            leave_game_server(state, server)
            {:noreply, %{state | server: game_id}}
        end
    end
  end

  def handle_cast({:left, game_id}, state) do
    case GameServer.exists?(game_id) do
      nil ->
        {:noreply, state}
      _ ->
        if state.server == game_id do
          {:noreply, %{state | server: nil}}
        else
          {:noreply, state}
        end
    end
  end

  def handle_info(:away, %{server: nil} = state) do
    {:stop, :normal, state}
  end

  def handle_info(:away, state) do
    leave_game_server(state, state.server)
    {:stop, :normal, state}
  end


  defp leave_game_server(state, game_id) do
    case GameServer.exists?(game_id) do
      nil -> :ok
      _ -> GameServer.leave(game_id, state.user_id)
    end
  end
end
