defmodule Sketch.Matchmaking do
  use GenServer

  require Logger

  import Sketch.Crdt.Helpers

  alias Sketch.GameCreator
  alias Sketch.GameServer
  alias Sketch.GameOptions


  @default_options %GameOptions{
    name: UUID.uuid4(:hex),
    turn_timeout: 90,
    reveal_time: 20,
    min_players: 2,
    autostart: true,
    allow_join: true,
    private: true,
    language: "en",
    max_round: 0
  }

  defmodule State do
    defstruct [:ets_table, :crdt]
  end


  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def child_spec(opts \\ []) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [opts]}
    }
  end

  def find(user_id, name) do
    GenServer.cast(__MODULE__, {:find, user_id, name})
  end


  def init(options) do
    crdt = Keyword.get(options, :crdt)
    :ets.new(__MODULE__, [:named_table, {:read_concurrency, true}])

    state = %State{
      ets_table: __MODULE__,
      crdt: crdt
    }

    {:ok, state}
  end


  def handle_cast({ :find, user_id, name }, state) do
    case find_available_games(state) |> Enum.sort_by(&elem(&1, 1)) do
      [] ->
        game_id = create_game(state)
        {:reply, game_id, state}
      [{game_id, _} | _] = games ->
        {:reply, game_id, {:continue, {:check_games, games}}}
    end
  end


  def handle_continue({:check_games, games}, state) do
    remaining = (10 * Enum.count(games)) - Enum.map(games, &elem(&1, 1)) |> Enum.sum

    # If 8 remaining places, create another game in prevision
    if remaining < 8 do
      create_game(state)
    end

    {:noreply, state}
  end

  def handle_info({:game_server, :init, id, count, _options}, state) do
    DeltaCrdt.mutate(state.crdt, :add, [id, count])
    {:noreply, state}
  end

  def handle_info({:game_server, :join, id, count}, state) do
    DeltaCrdt.mutate(state.crdt, :add, [id, count])
    {:noreply, state}
  end

  def handle_info({:game_server, :leave, id, count}, state) do
    DeltaCrdt.mutate(state.crdt, :add, [id, count])
    {:noreply, state}
  end

  def handle_info({:game_server, :terminate, id, reason}, state) do
    DeltaCrdt.mutate(state.crdt, :remove, [id])
    {:noreply, state}
  end

  def handle_info({:crdt_update, diffs}, state) do
    {:noreply, process_diffs(state, diffs, &process_diff/2)}
  end

  def handle_info({:EXIT, _pid, reason}, state) do
    {:stop, reason, state}
  end

  defp process_diff(state, {:add, id, value}) do
    :ets.insert(state.ets_table, {id, value})
    state
  end

  defp process_diff(state, {:remove, id}) do
    :ets.delete(state.ets_table, id)
    state
  end


  defp create_game(state) do
    game_id = GameCreator.generate_game_id()
    options = @default_options
    {:ok, _} = Sketch.GameCreator.instantiate_public_game({ game_id, options })

    game_id
  end


  defp find_available_games(state) do
    results = :ets.select(state.ets_table, [{
      {:"$1", :"$2"},
      [{ :<, :"$2", 10 }],
      [{{ :"$1", :"$2"}}]
    }])
  end

  defp join_game(state, game_id, user_id, name) do
    case GameServer.join(game_id, %{ user_id: user_id, name: name }) do
      {:ok, _} -> :ok
      {:error, :already_seated} -> :ok
      _ -> :error
    end
  end

  defp broadcast_to_user!(user_id, message, payload) do
    SketchWeb.Endpoint.broadcast("user:" <> user_id, message, payload)
  end
end

