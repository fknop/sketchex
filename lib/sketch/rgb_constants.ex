defmodule Sketch.RGBConstants do
  def colors do
    [
      "#ff8080",
      "#f2e780",
      "#fff080",
      "#ff80fb",
      "#80ffec",
      "#ffaa80",
      "#8093ff",
      "#b3ff80",
      "#ff80a6",
      "#5baa41"
    ]
  end
end

# #f2e780
# #5baa41
