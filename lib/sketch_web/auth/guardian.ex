defmodule SketchWeb.Auth.Guardian do
  use Guardian, otp_app: :sketch 

  alias Sketch.User, as: User


  def subject_for_token(user, _claims) do 
    {:ok, user.id}
  end 

  def build_claims(claims, user = %Sketch.User{}, _opts) do
    {:ok, claims
      |> Map.put("username", user.username)
      |> Map.put("type", :user)
    }
  end

  def build_claims(claims, resource, _opts) do
    {:ok, claims
      |> Map.put("username", resource.username)
      |> Map.put("type", :guest)}
  end 

  def resource_from_claims(claims) do
    {
      :ok, 
      %{ id: claims["sub"], username: claims["username"], type: claims["type"] }
    }
  end 

  def authenticate_guest(id, username) do
    guest = %{id: id, username: username}
    token = create_token(:guest, %{id: id, username: username })
    {:ok, token, guest}
  end

  def authenticate_user(email, password) do
    with {:ok, user} <- User.Query.get_by_email(email),
         true <- validate_password(password, user.password) do 
      token = create_token(:user, user)
      {:ok, token, user}
    else _ -> 
      {:error, :unauthorized}
    end
  end  

  defp create_token(:guest, guest) do 
    {:ok, token, _} = encode_and_sign(guest |> Map.put(:type, :guest))
    token
  end 

  defp create_token(:user, user) do
    {:ok, token, _} = encode_and_sign(user |> Map.put(:type, :user))
    token
  end


  defp validate_password(password, encrypted_password) do
    Bcrypt.verify_pass(password, encrypted_password)
  end
end