defmodule SketchWeb.Auth.JwtCookies do
  import Plug.Conn

  
  @cookie_hp "X-Sketch-id"
  @cookie_sig "X-Sketch-s"



  def get_cookies(conn) do
    cookies = conn 
    |> fetch_cookies
    |> Map.get(:req_cookies)

    if Map.has_key?(cookies, @cookie_hp) and Map.has_key?(cookies, @cookie_sig) do
      header_payload = cookies[@cookie_hp]
      signature = cookies[@cookie_sig]
      {header_payload, signature}
    else
      nil
    end
  end


  def put_jwt_header_payload(conn, header_payload) do
    conn 
      |> put_resp_cookie(@cookie_hp, header_payload, http_only: false, secure: true, max_age: 30 * 60)
  end

  def put_jwt_sig(conn, signature) do
    conn 
    |> put_resp_cookie(@cookie_sig, signature, http_only: true, secure: true)
  end
end