defmodule SketchWeb.Auth.Pipeline do
  use Guardian.Plug.Pipeline, otp_app: :sketch,
    module: SketchWeb.Auth.Guardian,
    error_handler: SketchWeb.Auth.ErrorHandler

  # plug SketchWeb.Plug.VerifyJwtCookies
  plug Guardian.Plug.VerifyHeader
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
  # plug SketchWeb.Plug.RenewJwtCookies
end