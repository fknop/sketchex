defmodule SketchWeb.Auth.ErrorHandler do
  import Plug.Conn

  def auth_error(conn, {type, _reason}, _opts) do

    {:ok, body} = Phoenix.json_library().encode(%{ reason: type, message: "Unauthorized." })

    conn 
      |> put_resp_content_type("application/json")
      |> send_resp(401, body)
  end
end