defmodule SketchWeb.Presence do
  use Phoenix.Presence, otp_app: :sketch,
                        pubsub_server: Sketch.PubSub


  def fetch("public:channel", entries) do
    for {key, %{metas: metas}} <- entries, into: %{} do
      {key, %{metas: metas |> Enum.map(&(Map.delete(&1, :user_id))), count: length(metas) }}
    end
  end

  def fetch(_, entries), do: entries
end
