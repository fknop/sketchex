defmodule SketchWeb.UserChannel do
  use SketchWeb, :channel

  def join("user:" <> user_id, _params, socket) do
    if authenticated?(socket) and socket.assigns[:user_id] == user_id do
      {:ok, socket}
    else
      :error
    end
  end

  defp authenticated?(socket), do: Guardian.Phoenix.Socket.authenticated?(socket)
end
