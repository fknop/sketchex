defmodule SketchWeb.PublicChannel do
  use SketchWeb, :channel

  require Logger

  alias SketchWeb.Presence
  alias Sketch.PlayerSession

  import Sketch.Crypto

  def join("public:channel", _params, socket) do
    if authenticated?(socket) do
      send(self(), :after_join)
      # Stop the away timer
      Logger.info(inspect {:session, :stop_away, socket.assigns[:user_id]})
      PlayerSession.stop_away(socket.assigns[:user_id])
      {:ok, socket}
    else
      :error
    end
  end

  def handle_info(:after_join, socket) do
    {:ok, _} = Presence.track(socket, :users, %{user_id: socket.assigns[:user_id]})
    push(socket, "presence_state", Presence.list(socket))
    {:noreply, socket}
  end

  def terminate(_reason, socket) do
    Logger.debug(inspect {:session, :start_away, socket.assigns[:user_id]})
    PlayerSession.start_away(socket.assigns[:user_id])
  end

  defp authenticated?(socket), do: Guardian.Phoenix.Socket.authenticated?(socket)
end
