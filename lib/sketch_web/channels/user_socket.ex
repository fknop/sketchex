defmodule SketchWeb.UserSocket do
  use Phoenix.Socket

  require Logger

  alias Sketch.PlayerSession

  channel "public:channel", SketchWeb.PublicChannel
  channel "public:browser", SketchWeb.BrowserChannel
  channel "user:*", SketchWeb.UserChannel
  channel "draw:*", SketchWeb.DrawChannel

  ## Channels
  # channel "room:*", SketchWeb.RoomChannel

  # Socket params are passed from the client and can
  # be used to verify and authenticate a user. After
  # verification, you can put default assigns into
  # the socket that will be set for all channels, ie
  #
  #     {:ok, assign(socket, :user_id, verified_user_id)}
  #
  # To deny connection, return `:error`.
  #
  # See `Phoenix.Token` documentation for examples in
  # performing token verification on connect.
  def connect(%{ "token" => token }, socket, _connect_info) do
    case Guardian.Phoenix.Socket.authenticate(socket, SketchWeb.Auth.Guardian, token) do
      {:ok, socket} ->

        resource = Guardian.Phoenix.Socket.current_resource(socket)
        user_id = resource[:id]
        name = resource[:username]

        unless PlayerSession.exists?(user_id) do
          Logger.debug(inspect { :session, :create, user_id })
          {:ok, pid} = Horde.Supervisor.start_child(Sketch.GlobalSupervisor, PlayerSession.child_spec({ user_id, :guest }))
          Logger.info(inspect pid)
        end

        {:ok, socket |> assign(:user_id, user_id) |> assign(:name, name)}
      {:error, value} ->
        Logger.debug(inspect value)
        :error
    end
  end


  # Socket id's are topics that allow you to identify all sockets for a given user:
  #
  #     def id(socket), do: "user_socket:#{socket.assigns.user_id}"
  #
  # Would allow you to broadcast a "disconnect" event and terminate
  # all active sockets and channels for a given user:
  #
  #     SketchWeb.Endpoint.broadcast("user_socket:#{user.id}", "disconnect", %{})
  #
  # Returning `nil` makes this socket anonymous.
  def id(socket), do: "user_socket:#{socket.assigns[:user_id]}"
end
