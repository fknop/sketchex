defmodule SketchWeb.DrawChannel do
  use SketchWeb, :channel

  require Logger
  require Sketch.Macros

  alias Sketch.GameServer
  alias Sketch.Macros

  Macros.dev_mode do
    intercept ["end_migration"]
  end

  def join("draw:" <> id, _params, socket) do
    if authenticated?(socket) do
      with pid when not is_nil(pid) <- GameServer.exists?(id),
          {:ok, _} <- GameServer.join(id, %{ user_id: socket.assigns[:user_id], name: socket.assigns[:name] })
      do
        state = GameServer.get_initial_state(id, socket.assigns[:user_id])
        {:ok, state, assign(socket, :game_id, id)}
      else
        {:error, :already_seated} ->
          state = GameServer.get_initial_state(id, socket.assigns[:user_id])
          {:ok, state, assign(socket, :game_id, id)}
        {:error, :server_full} ->
          {:error, %{ reason: :server_full }}
        {:error, :cannot_join_in_progress} ->
          {:error, %{ reason: :cannot_join_in_progress }}
        nil ->
          {:error, %{ reason: :server_not_found}}
        _ ->
          {:error, %{ reason: :unknown }}
      end
    else
      {:error, %{ reason: :unauthorized }}
    end
  end

  def handle_in("start", _message, socket) do
    response = GameServer.start_game(socket.assigns[:game_id], socket.assigns[:user_id])
    {:reply, response, socket}
  end

  def handle_in("draw", %{ "event" => [[_ | _] | _] = events }, socket) do
    for event <- events do
      GameServer.draw_event(socket.assigns[:game_id], socket.assigns[:user_id], event)
    end
    {:noreply, socket}
  end

  def handle_in("draw", %{ "event" => event }, socket) do
    GameServer.draw_event(socket.assigns[:game_id], socket.assigns[:user_id], event)
    {:noreply, socket}
  end


  def handle_in("fill", %{ "event" => event }, socket) do
    GameServer.fill_event(socket.assigns[:game_id], socket.assigns[:user_id], event)
    {:noreply, socket}
  end

  def handle_in("undo", _message, socket) do
    GameServer.undo(socket.assigns[:game_id], socket.assigns[:user_id])
    {:noreply, socket}
  end

  def handle_in("redo", _message, socket) do
    GameServer.redo(socket.assigns[:game_id], socket.assigns[:user_id])
    {:noreply, socket}
  end

  def handle_in("checkpoint", _message, socket) do
    GameServer.checkpoint(socket.assigns[:game_id], socket.assigns[:user_id])
    {:noreply, socket}
  end

  def handle_in("chat", %{ "message" => message }, socket) do
    GameServer.chat_message(socket.assigns[:game_id], socket.assigns[:user_id], message)
    {:noreply, socket}
  end

  def handle_in("pass", _message, socket) do
    GameServer.pass(socket.assigns[:game_id], socket.assigns[:user_id])
    {:noreply, socket}
  end

  def handle_in("choose_word", %{ "word" => word }, socket) do
    GameServer.choose_word(socket.assigns[:game_id], socket.assigns[:user_id], word)
    {:noreply, socket}
  end

  def handle_in("clear", _message, socket) do
    GameServer.clear(socket.assigns[:game_id], socket.assigns[:user_id])
    {:noreply, socket}
  end

  def terminate({:shutdown, :left}, socket) do
    if GameServer.exists?(socket.assigns[:game_id]) do
      GameServer.leave(socket.assigns[:game_id], socket.assigns[:user_id])
    end
  end

  def terminate(_reason, _socket) do
  end

  Macros.dev_mode do
    def handle_out("end_migration", payload, socket) do

      Logger.debug("HANDLE_OUT: END_MIGRATION: #{inspect(socket.assigns[:name])}")


      push(socket, "end_migration", payload)

      {:noreply, socket}
    end
  end

  defp authenticated?(socket), do: Guardian.Phoenix.Socket.authenticated?(socket)
end
