defmodule SketchWeb.BrowserChannel do
  use SketchWeb, :channel

  def join("public:browser", _params, socket) do
    if authenticated?(socket) do
      {:ok, socket}
    else 
      :error
    end
  end

  # Add authorization logic here as required.
  defp authenticated?(socket), do: Guardian.Phoenix.Socket.authenticated?(socket)
end
