defmodule SketchWeb.Params.Pagination do
  use SketchWeb, :params

  @primary_key false
  embedded_schema do
    field :page, :integer, default: 1
    field :limit, :integer, default: 10 
  end

  def changeset(struct, params \\ %{}) do
    struct 
    |> cast(params, [:page, :limit])
    |> validate_number(:page, greater_than: 0)
    |> validate_number(:limit, greater_than: 0)
    |> validate_number(:limit, less_than_or_equal_to: 30)
  end
end