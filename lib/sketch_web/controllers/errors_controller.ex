defmodule SketchWeb.Api.ErrorsController do
  use SketchWeb, :controller

  import SketchWeb.ControllerHelpers

  plug Sketch.Error, :error when action in [:create]

  def create(conn, _params) do
    error = conn.assigns[:error]
    Sketch.Repo.insert(error)
    ok(conn)
  end
end