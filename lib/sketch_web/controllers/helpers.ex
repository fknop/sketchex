defmodule SketchWeb.ControllerHelpers do
  import Phoenix.Controller
  import Plug.Conn
  

  def error(reason, message), do: %{reason: reason, message: message}

  def missing_parameters(), do: error(:missing, "Parameters are missing")
  def not_found_error(message), do: error(:not_found, message)

  def reply(conn, body), do: json conn, body
  def reply(conn, body, status), do: json put_status(conn, status), body

  def ok(conn, body \\ ""), do: conn |> json(body)
  def created(conn, body \\ ""), do: conn |> put_status(:created) |> json(body)
  def bad_request(conn, body \\ "Bad request"), do: conn |> put_status(:bad_request) |> json(body)
  def unauthorized(conn, body \\ ""), do: conn |> put_status(:unauthorized) |> json(body)
  def forbidden(conn, body \\ ""), do: conn |> put_status(:forbidden) |> json(body)
  def not_found(conn, body \\ ""), do: conn |> put_status(:not_found) |> json(%{ error: "Not found", reason: body })
  def internal_server_error(conn, body \\ ""), do: conn |> put_status(:internal_server_error) |> json(%{ error: "Internal server error", reason: body })
end