defmodule SketchWeb.Api.UserController do
  use SketchWeb, :controller

  import SketchWeb.ControllerHelpers

  alias Sketch.User

  plug SketchWeb.Plug.RequireUUID, params: [:id]

  def show(conn, %{"id" => id}) do
    case User.Query.get_by_id(id) do 
      {:ok, user} -> 
        reply conn, user
      {:error, :not_found} ->
        reply conn, %{}, :not_found
    end
  end

end