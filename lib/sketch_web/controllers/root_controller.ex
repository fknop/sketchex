defmodule SketchWeb.Api.RootController do
  use SketchWeb, :controller

  import SketchWeb.ControllerHelpers

  def health(conn, _params) do
    reply conn, %{ :health => "ok" }
  end

end
