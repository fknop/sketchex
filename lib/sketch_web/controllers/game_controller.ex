defmodule SketchWeb.Api.GameController do
  use SketchWeb, :controller

  import SketchWeb.ControllerHelpers
  import Sketch.Crypto

  alias Sketch.GameServer

  plug SketchWeb.Params.Pagination, :pagination when action in [:index]
  plug Sketch.GameOptions, :options when action in [:create]

  def index(conn, _params) do
    page = conn.assigns[:pagination].page
    limit = conn.assigns[:pagination].limit
    response = Sketch.GameLookup.list(page, limit)
    ok(conn, response)
  end

  def show(conn, %{"id" => id}) do
    case GameServer.exists?(id) do
      nil ->
        not_found(conn, "Game #{id} not found.")
      _ ->
        options = GameServer.get_options(id)
        ok(conn, %{ id: id, options: options })
    end
  end

  def create(conn, _params) do

    resource = Guardian.Plug.current_resource(conn)
    game_id = Sketch.GameCreator.generate_game_id()
    options = conn.assigns[:options]

    with {:ok, _ } <- Sketch.GameCreator.instantiate({ game_id, options.name, resource.id, options })
    do
      ok(conn, %{ game_id: game_id })
    else {:error, {reason, _}} ->
      internal_server_error(conn, reason)
    end
  end
end
