defmodule SketchWeb.Api.AuthController do
  use SketchWeb, :controller
  import Phoenix.Controller
  import Sketch.Crypto
  import SketchWeb.ControllerHelpers
  import Sketch.EctoHelpers

  alias SketchWeb.Auth.Guardian
  alias SketchWeb.Auth.JwtCookies
  alias SketchWeb.Auth
  alias Sketch.User

  # Move errors to fallback controller
  def signup(conn, params) do
    with {:ok, user} <- User.Query.create_user(params),
         {:ok, token, _} <- Guardian.encode_and_sign(user) do
      created(conn, %{token: token, id: user.id, username: user.username})
    else
      {:error, changeset = %Ecto.Changeset{}} ->
        reply conn, %{errors: error_map(changeset)}, error_status(changeset)
    end
  end

  def signin(conn, %{"email" => email, "password" => password}) do
    case Auth.Guardian.authenticate_user(email, password) do
      {:error, :unauthorized} ->
        unauthorized(conn, error(:invalid, "Email or password is invalid"))
      {:ok, token, user} ->
        # [header, payload, signature] = String.split(token, ".")
        conn
        # |> JwtCookies.put_jwt_header_payload("#{header}.#{payload}")
        # |> JwtCookies.put_jwt_sig(signature)
        |> ok(%{ token: token, id: user.id, username: user.username })
    end
  end

  def signin(conn, %{"username" => username}) do
    uuid = player_uuid() # Temporary id
    {:ok, token, guest} = Auth.Guardian.authenticate_guest(uuid, username)

    # [header, payload, signature] = String.split(token, ".")

    conn
      # |> JwtCookies.put_jwt_header_payload("#{header}.#{payload}")
      # |> JwtCookies.put_jwt_sig(signature)
      |> ok(%{ token: token, id: uuid, username: guest.username })
  end

  def signin(conn, _params), do: bad_request(conn)
end
