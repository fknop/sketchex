defmodule SketchWeb.Router do
  use SketchWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :auth do
    plug SketchWeb.Auth.Pipeline
  end


  scope "/", SketchWeb do
    pipe_through [:api]
    get "/", Api.RootController, :health
    get "/health", Api.RootController, :health
  end


  scope "/api", SketchWeb do
    pipe_through :api
    post "/signin", Api.AuthController, :signin
    post "/signup", Api.AuthController, :signup
  end

  scope "/api", SketchWeb do
    pipe_through [:api, :auth]

    resources "/games", Api.GameController, only: [:index, :show, :create]
    resources "/users", Api.UserController, only: [:show]
    resources "/draw", Api.DrawController, only: [:create, :show]

  end

  scope "/api", SketchWeb do
    pipe_through [:api, :auth]
    resources "/errors", Api.ErrorsController, only: [:create]
  end


end
