defmodule SketchWeb.Plug.RequireUUID do
  import Plug.Conn

  def init(options \\ [params: [:id]]), do: options

  def call(conn, options) do
    [params: keys] = options
    params = Enum.map(keys, &(conn.params[Atom.to_string(&1)]))

    if Enum.all?(params, &is_uuid?/1) do 
      conn
    else
      conn
        |> put_resp_content_type("application/json")
        |> send_resp(404, "") 
        |> halt
    end
  end

  defp is_uuid?(param) do
    case Ecto.UUID.dump(param) do 
      :error -> false
      {:ok, _} -> true
    end
  end
end