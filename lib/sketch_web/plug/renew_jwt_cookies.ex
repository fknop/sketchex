defmodule SketchWeb.Plug.RenewJwtCookies do
  alias SketchWeb.Auth.JwtCookies
  
  def init(options \\ []), do: options

  def call(conn, _options) do
    Plug.Conn.register_before_send(conn, fn conn ->
      case JwtCookies.get_cookies(conn) do
        nil -> 
          conn
        {hp, _} -> 
          conn |> JwtCookies.put_jwt_header_payload(hp)
      end
    end)
  end
end