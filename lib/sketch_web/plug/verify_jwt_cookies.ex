defmodule SketchWeb.Plug.VerifyJwtCookies do
  import Plug.Conn
  alias SketchWeb.Auth.JwtCookies
  
  def init(options \\ []), do: options

  def call(conn, _options) do
    case JwtCookies.get_cookies(conn) do
      nil -> 
        conn
      {header_payload, signature} ->
        put_req_header(conn, "authorization", "Bearer #{header_payload}.#{signature}")  
    end
  end
end